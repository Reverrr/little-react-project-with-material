interface initialState {
    someDataForTransfer: string,
    headerColorLoad: string
}

interface actionModel<T> {
    type: string,
    payload: T
}

const initialState = {
    someDataForTransfer: '',
    headerColorLoad: '#fff'
}

const reducer = (state: initialState = initialState, action: actionModel<any>) => {
    switch (action.type) {
        case 'SOME_DATA_FOR_TRANSFER': {
            return {
                ...state,
                someDataForTransfer: action.payload
            };
        }
        case 'HEADER_COLOR_LOAD': {
            return {
                ...state,
                headerColorLoad: action.payload
            };
        }
        default: {
            return state;
        }
    }
}

export default reducer;
