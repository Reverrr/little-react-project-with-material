import React, { Component } from 'react';
import './App.scss';
import Header from './header/header';
import SettingComponent from './setting/setting.component';
import Home from './home/home';
import Footer from './footer/footer';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import LoginComponent from './auth/login/login.component';
import RegisterComponent from './auth/register/register.component';
import { firebaseApp } from './shared/helpers/firebase.helper';
import { createBrowserHistory } from "history";

const firebaseAppAuth = firebaseApp.auth();

class App extends Component<any, any> {

  public state = {
    user: null
  }

  async componentDidMount() {
    firebaseAppAuth.onAuthStateChanged((user) => {
        this.setState({user: user});
    }, (err) => {console.log(err)});
  }

  render() {
    return (
      <div className="App">
        <Header history={createBrowserHistory()} />
      <Switch>
        {console.log(this.state.user)}
        {(!this.state.user) ? (
          <>
            <Route path="/auth/login" component={LoginComponent} />
            <Route path="/auth/register" component={RegisterComponent} />
            <Redirect to='/auth/login' />
          </>
        ) : (
          <>
            <Route exact path="/" component={Home} />
            <Route path="/setting" component={SettingComponent} />
            <Redirect to='/' />
          </>
        )}
      </Switch>
      <Footer />
      </div>
    );
  }
}

export default App;
