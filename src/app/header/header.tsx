import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { someDataLoaded, headerColorLoad } from '../../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ColorSettingService } from '../shared/services/color-setting.service';
import { AppBar, Toolbar, IconButton, Typography, Button, Tabs, Tab } from '@material-ui/core';
import './header.scss'; 
import { firebaseApp } from '../shared/helpers/firebase.helper';

const settingColor = new ColorSettingService();

const firebaseAppAuth = firebaseApp.auth();
interface StateInterface {
  email: string
}

class Header extends Component<any, any> {

  public history;

  constructor(props) {
      super(props);
  }
  public state: StateInterface = {
    email: ''
  }
  async componentDidMount() {
    firebaseAppAuth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({email: user.email});
      } else {
        // No user is signed in.
      }
    });
  }
  public render() {
    // const classes = useStyles();
    const props: any = this.props;

    const logout = async () => {
      const logoutData = await firebaseAppAuth.signOut();
      this.props.history.go('/auth/login');
    }

    return (
      <div>
        <AppBar position="static" style={{background: `${(settingColor.getColorTheme()) ? settingColor.getColorTheme() : props.color}`}}>
          <Toolbar>
            <IconButton edge="start" color="inherit" aria-label="menu">
            </IconButton>
            <Typography variant="h6" >
              ReactDOM
            </Typography>
            <Link to="/"><Button color="inherit">Home</Button></Link>
            <Link to="/setting"><Button color="inherit">Setting</Button></Link>
            <div className="right-component">
            <h3 className="mr-4">
              {(this.state.email) ? this.state.email : ''}
            </h3>
              <Button color="inherit" onClick={logout}>LogOut</Button>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  
  return {
    someDataForTransfer: state.someDataForTransfer,
    color: state.headerColorLoad
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    someDataLoaded,
    headerColorLoad
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
