import React, { Component } from 'react';
import { ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails, GridList, GridListTile, GridListTileBar, IconButton, createStyles, makeStyles, Theme, Popover } from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import './home.scss';
import { ColorHelper } from '../shared/helpers/color.helper';
import { ColorSettingService } from '../shared/services/color-setting.service';
import { headerColorLoad } from '../../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const settingColor = new ColorSettingService();

const tileData = [
  {
    img: '/static/images/breakfast.jpg',
    title: 'Breakfast',
    author: 'jill111',
    cols: 2,
    featured: true,
  },
  {
    img: '/static/images/burgers.jpg',
    title: 'Tasty burger',
    author: 'director90',
  },
  {
    img: '/static/images/camera.jpg',
    title: 'Camera',
    author: 'Danson67',
  },
  {
    img: '/static/images/morning.jpg',
    title: 'Morning',
    author: 'fancycrave1',
    featured: true,
  },
  {
    img: '/static/images/hats.jpg',
    title: 'Hats',
    author: 'Hans',
  },
  {
    img: '/static/images/honey.jpg',
    title: 'Honey',
    author: 'fancycravel',
  },
  {
    img: '/static/images/vegetables.jpg',
    title: 'Vegetables',
    author: 'jill111',
    cols: 2,
  },
  {
    img: '/static/images/plant.jpg',
    title: 'Water plant',
    author: 'BkrmadtyaKarki',
  },
  {
    img: '/static/images/mushroom.jpg',
    title: 'Mushrooms',
    author: 'Jeka228',
  },
  {
    img: '/static/images/olive.jpg',
    title: 'Olive oil',
    author: 'congerdesign',
  },
  {
    img: '/static/images/star.jpg',
    title: 'Sea star',
    cols: 2,
    author: '821292',
  },
  {
    img: '/static/images/bike.jpg',
    title: 'Bike',
    author: 'danfador',
  },
];

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
    gridList: {
      width: '100%',
      height: '100%',
    },
    icon: {
      color: 'rgba(255, 255, 255, 0.54)',
    },
    typography: {
      padding: theme.spacing(2),
    },
    popover: {
      pointerEvents: 'none',
    },
    paper: {
      padding: theme.spacing(1),
    },
  }),
);
let selectedPopover;

const ListProduct: React.FC = () => {

  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const open = Boolean(anchorEl);

  function handlePopoverOpen(event: React.MouseEvent<HTMLElement, MouseEvent>) {
    selectedPopover = event.currentTarget.id;
    setAnchorEl(event.currentTarget);
  }

  function handlePopoverClose() {
    setAnchorEl(null);
  }
  
  const classes = useStyles({});
  return (
    <div>
      <div className={classes.root}>
        <GridList cols={4} cellHeight={220} className={classes.gridList}>
          {tileData.map((tile, index) => (
            <GridListTile cols={(index % 2 || index % 3) ? 1 : 3} key={tile.img}>
              <img src={tile.img} alt={tile.title} />
              <GridListTileBar
                title={tile.title}
                subtitle={<span>by: {tile.author}</span>}
                actionIcon={
                  <IconButton
                  id={index.toString()}
                  onMouseEnter={handlePopoverOpen}
                  onMouseLeave={handlePopoverClose}
                  aria-label={`info about ${tile.title}`} className={classes.icon}>
                    <InfoIcon />
                    <Popover
                      id={index.toString()}
                      className={classes.popover}
                      classes={{
                        paper: classes.paper,
                      }}
                      open={(open && +selectedPopover === index) ? true : false}
                      anchorEl={anchorEl}
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                      }}
                      transformOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                      }}
                      onClose={handlePopoverClose}
                      disableRestoreFocus
                    >
                      <Typography>{tile.title}</Typography>
                    </Popover>
                  </IconButton>
                }
              />
            </GridListTile>
          ))}
        </GridList>
      </div>
    </div>
  );
}


class Home extends Component<any> {
  
  render() {

    let props = this.props;
    let color;
    if (settingColor.getColorTheme()) {
      color = ColorHelper.toLight(settingColor.getColorTheme());
    } else {
      color = props.color;
    }
    
    return (
      <div>
        <div className="expansion-panel" style={{background: `${color}`}}>
          <ExpansionPanel>
          <ExpansionPanelSummary
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography style= {{color: `${color}`}}>Expansion Panel 1</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
              sit amet blandit leo lobortis eget.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography style= {{color: `${color}`}}>Expansion Panel 2</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
              sit amet blandit leo lobortis eget.
              </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary
            aria-controls="panel3a-content"
            id="panel3a-header"
          >
          <Typography style= {{color: `${color}`}}>Disabled Expansion Panel</Typography>
          </ExpansionPanelSummary>
        </ExpansionPanel>

        </div>
        <ListProduct />
      </div>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    someDataForTransfer: state.someDataForTransfer,
    color: state.headerColorLoad
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    headerColorLoad
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
