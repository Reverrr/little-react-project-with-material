import React, { Component } from "react";
import { Input, Button, Toolbar } from '@material-ui/core';
import { firebaseApp } from "../../shared/helpers/firebase.helper";
import { Link } from "react-router-dom";

interface StateIntertface {
    email: string;
    password: string;
}

// const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAppAuth = firebaseApp.auth();

// const LinkComponent = () => {
//     return (
//         <div>
//             <Link to="/auth/login"><p>AAAAA</p></Link>
//         </div>
//     );
// }

class RegisterComponent extends Component<any, StateIntertface> {
    constructor(props) {
        super(props);
    
        this.state = {
            email: "",
            password: ""
        };
  }

  validateForm() {
    // return this.state.email. > 0 && this.state.password.length > 0;
  }

  handleSubmit = event => {
    event.preventDefault();
  }
  
  render() {

    const handleChange = (event: any) => {
        const name: string = event.target.name;
        let obj: any = Object.assign({}, this.state);
        obj[name] = event.target.value;
        this.setState({...obj});
    }

    const submitEvent = async (event: any) => {
        const login = await firebaseAppAuth.createUserWithEmailAndPassword(this.state.email, this.state.password);
    }

    return (
      <div className="auth h-100">
          <h3>Register</h3>
        <form onSubmit={this.handleSubmit}>
          <Input name="email" type="text" onChange={handleChange} value={this.state.email} placeholder="email" />
          <Input name="password" type="text" onChange={handleChange} value={this.state.password} placeholder="password" />
          <Button color="primary" children="Send" value="Send" onClick={submitEvent} />
        </form>
        <Link style={{color: '#000'}} to="/auth/login">To Login</Link>
      </div>
    );
  }
}


export default RegisterComponent;