import React, { Component } from "react";
import { Input, Button } from '@material-ui/core';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { firebaseApp } from "../../shared/helpers/firebase.helper";
import { Link, Redirect } from "react-router-dom";
import FirebaseService from '../../shared/services/firebase.service';

interface StateIntertface {
    email: string;
    password: string;
}

const fireBase = new FirebaseService();

class LoginComponent extends Component<any, StateIntertface> {
    constructor(props) {
        super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  validateForm() {
    // return this.state.email. > 0 && this.state.password.length > 0;
  }

  handleSubmit = event => {
    event.preventDefault();
  }
  
  render() {
    const firebaseAppAuth = firebaseApp.auth();

    const handleChange = (event: any) => {
        const name: string = event.target.name;
        let obj: any = Object.assign({}, this.state);
        obj[name] = event.target.value;
        this.setState({...obj});
    }

    const submitEvent = async (event: any) => {
        const login = await firebaseAppAuth.signInWithEmailAndPassword(this.state.email, this.state.password);
        if (login) {
            this.props.history.push('/');
        }
    }
    const tempfirebaseAppAut = JSON.stringify(firebaseAppAuth);

    const tempfirebaseAppAuth: any = {...firebaseAppAuth};
    return (
      <div className="auth">
      <h3>Login</h3>
        <form onSubmit={this.handleSubmit}>
          <Input name="email" type="text" onChange={handleChange} value={this.state.email} placeholder="email" />
          <Input name="password" type="text" onChange={handleChange} value={this.state.password} placeholder="password" />
          <Button color="primary" children="Send" value="Send" onClick={submitEvent} />
        </form>
        <Link style={{color: '#000'}} to="/auth/register">To Register</Link>
      </div>
    );
  }
}


export default LoginComponent;