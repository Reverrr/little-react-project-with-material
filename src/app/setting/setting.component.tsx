import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { someDataLoaded, headerColorLoad } from '../../actions/actions';
import { connect } from 'react-redux';
import { SketchPicker } from 'react-color';
import { ColorSettingService } from '../shared/services/color-setting.service';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';

const settingColor = new ColorSettingService();

class SettingComponent extends Component<any> {

  public state = {
    someData: '',
    color: 'red',
    fruit: '',
    fruits: ['asd', 'sdss']
  }

  public render() {
    const props = this.props;
    const handleChange = async (event: any) => {
      const name = event.target.name;
      await this.setState({
          [name]: event.target.value
      });
    }

    return (
        <Container fixed>
          <div className="setting-form">
            <TextField
              id="outlined-name"
              label="Who are u?"
              className="input"
              value={this.state.someData}
              onChange={handleChange}
              margin="normal"
              name="someData"
              variant="outlined"
            />
            <Button onClick={(event) => {
              event.preventDefault();
              props.someDataLoaded(this.state.someData);
            }} variant="contained" color="primary">Submit</Button>
          </div>
            <SketchPicker
            color={ this.state.color }
            onChange={(event: any) => {
              props.headerColorLoad(event.hex);
              settingColor.setColorTheme(event.hex);
            }} />
        </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  
  return {
    someDataForTransfer: state.someDataForTransfer,
    color: state.headerColorLoad
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    someDataLoaded,
    headerColorLoad
  }, dispatch);
}

export default (connect(mapStateToProps, mapDispatchToProps)(SettingComponent));
