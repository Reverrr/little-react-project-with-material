import {hexToRgba, rgbaToHex} from 'hex-and-rgba';

export class ColorHelper {
    /**
     * 
     * @param hex - hex color
     * @param to to percent
     * @param opacity opacity opacity
     */
    public static toDarkness(hex: string, to: number = 40, opacity = 1): string {
        const [red,green,blue] = hexToRgba(hex);

        const resultRed = Math.floor(red - (red / 100 * to));
        const resultGreen = Math.floor(green - (green / 100 * to));
        const resultBlue = Math.floor(blue - (blue / 100 * to));
        
        return rgbaToHex(resultRed, resultGreen, resultBlue, opacity);
    }
    
    /**
     * 
     * @param hex - hex color
     * @param to to percent
     * @param opacity opacity opacity
     */
    public static toLight(hex: string, to: number = 40, opacity = 1): string {
        const [red,green,blue] = hexToRgba(hex);
        // const red = hexToRgba(hex)[0];
        // const green = hexToRgba(hex)[1];
        // const blue = hexToRgba(hex)[2];

        const resultRed = Math.floor(red + ((255 - red) / 100 * to));
        const resultGreen = Math.floor(green + ((255 - green) / 100 * to));
        const resultBlue = Math.floor(blue + ((255 - blue) / 100 * to));
        
        return rgbaToHex(resultRed, resultGreen, resultBlue, opacity);
    }
}