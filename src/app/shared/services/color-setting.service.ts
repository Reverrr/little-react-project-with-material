export class ColorSettingService {
    constructor() {}
    /**
     * @param hex set hex color
     */
    public setColorTheme(hex: string) {
        localStorage.setItem('colorTheme', JSON.stringify(hex));
    }
    public getColorTheme() {
        return JSON.parse(localStorage.getItem('colorTheme'));
    }
}
