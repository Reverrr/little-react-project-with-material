import { firebaseApp } from "../helpers/firebase.helper";


export default class FirebaseService {

    public firebaseAppAuth = firebaseApp.auth();

    private getUserData() {
        return this.firebaseAppAuth.currentUser;
    }
}