import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { someDataLoaded, headerColorLoad } from '../../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ColorSettingService } from '../shared/services/color-setting.service';
import { AppBar, Toolbar, IconButton, Typography, Button, Tabs, Tab } from '@material-ui/core';
import './footer.scss';
import { ColorHelper } from '../shared/helpers/color.helper';


const settingColor = new ColorSettingService();


class Footer extends Component<any> {
  
  public render() {
    const props: any = this.props;
    
    let background;
    if (settingColor.getColorTheme()) {
      background = ColorHelper.toDarkness(settingColor.getColorTheme());
    } else {
      background = props.color;
    }

    return (
      <footer className="footer"> 
        <AppBar position="static" style={{background: `${background}`}}>
          <Toolbar>
            <IconButton edge="start" color="inherit" aria-label="menu">
            </IconButton>
            <Typography variant="h6" >
              Footer
            </Typography>
            <Link to="/"><Button color="inherit">Home</Button></Link>
            <Link to="/setting"><Button color="inherit">Setting</Button></Link>
            <div className="right-component">
              <Button color="inherit">{props.someDataForTransfer}</Button>
            </div>
          </Toolbar>
        </AppBar>
      </footer>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    someDataForTransfer: state.someDataForTransfer,
    color: state.headerColorLoad
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    someDataLoaded,
    headerColorLoad
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
