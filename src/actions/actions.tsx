const someDataLoaded = (data: any) => {
    return {
        type: 'SOME_DATA_FOR_TRANSFER',
        payload: data
    }
}

const headerColorLoad = (data: string) => {
    return {
        type: 'HEADER_COLOR_LOAD',
        payload: data
    }
}


export {
    someDataLoaded,
    headerColorLoad
}
